# Ansible role: Docker
An Ansible Role to:
 - Install docker
 - Install docker-compose
 - Install docker and docker-compose python packages ( required by ansible docker modules )
 - Login to Scaleway internal registry ( http://docker-registry.infra.online.net and http://docker-proxy.infra.online.net)

## Usage

### requirement.yml
```
- name: docker
  src: https://gitlab.infra.online.net/ci-cd/ansible/docker
```

### site.yml
```yml
 - name: Install docker
   hosts: all
   roles:
     - docker
```

## Why don't we use galaxy role

We don't use galaxy role for several reasons:
  - It does not allow to install a fixed version of docker
  - Somme configuration MUST be changed in docker daemon to avoid network ip range conflict
  - It does not log you on internal Scaleway registry
  
## Available variables

All role variables can be found in [./defaults/main.yml](./defaults/main.yml)

## Gotcha

Version mapping between `apt` package name and docker version cannot be automatic.
Version binding can be found in [./vars/main.yml](./vars/main.yml).
Feel free to add missing version if needed.

## Contribute

If you find an bug or a missing feature please [open a gitlab issue](https://gitlab.infra.online.net/ci-cd/ansible/docker/issues/new)
MR are welcome ;-)

## Stability

This module is flagged as **preview** which means that it is not guaranteed to have a backwards compatible interface.

## Maintainer
  - Jérôme Quéré ([@jquere](https://scaleway.slack.com/app_redirect?channel=U98KYL3BR) 
  - Olivier Cano ([@ocano](https://scaleway.slack.com/app_redirect?channel=UCVV4DMRD)